import os
import requests
import base64


with open(os.environ['DATASCIENCE']+"/credentials.txt") as f:
    content = f.readlines()
content = [x.strip() for x in content] 

client_id = content[0]
client_secret = content[1]
user_token = content[2]
user_token_secret = content[3]

class AuthenticationToken:

	def __init__(self, client_id, client_secret):
		self.client_id = client_id
		self.client_secret = client_secret
		self.access_token = self.__get_access_token();
		self.header = {"Authorization" : "Bearer {}".format(self.access_token), 'Accept-Encoding': 'gzip'}
		print("initialized AuthenticationToken")
		
	def __get_access_token(self):
		credentials = self.client_id+":"+self.client_secret

		# encode credentials with base64
		base64EncodedCredentials = base64.b64encode(credentials.encode()).decode()
		params = {"grant_type":"client_credentials"}
		header = {"Content-Type":"application/x-www-form-urlencoded;charset=UTF-8",
		"Authorization":"Basic " + base64EncodedCredentials}
		print("calling twitter auth")
		r = requests.post("https://api.twitter.com/oauth2/token", data = params, headers = header)
		if r.status_code == requests.codes.ok:
			return r.json()["access_token"]
		else:
			raise IOError("Could not access Twitter. Maybe invalid credentials?")


tokenApi = AuthenticationToken(client_id,client_secret)
def getAccessToken():
	# print("Access token: %s"% tokenApi.access_token)
	return tokenApi.access_token
# getAccessToken()


print("got token: "+getAccessToken())