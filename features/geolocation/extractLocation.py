import ast
import os
import json
import time
from datetime import datetime, timedelta
import sys
import csv
import re
import pprint
import requests

# sys.path.insert(0,os.environ['DATASCIENCE'])
# import requests
# from apis import TwitterAPIs

pp = pprint.PrettyPrinter(indent=2)

def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total:
        print()

def fetchLocation(userInfo):
    # Default Latitude,Longitude for ucl final
    location = '50.4334058,30.521856'

    if 'location' in userInfo and userInfo['location'] != '':
        # pp.pprint(userInfo['location'])

        # Make the same request we did earlier, but with the coordinates of San Francisco instead.
        parameters = { 'location': userInfo['location'], 'key': 'lYrP4vF3Uk5zgTiGGuEzQGwGIVDGuy24' }
        response = requests.get("http://www.mapquestapi.com/geocoding/v1/address", params=parameters)

        # Get the response data as a python object.  Verify that it's a dictionary.
        data = response.json()

        if 'results' in data and len(data['results']) > 0:
            location = data['results'][0]

            if 'locations' in location and len(location['locations']) > 0:
                prefferedLocation = location['locations'][0]

                if 'latLng' in prefferedLocation:
                    location = str(prefferedLocation['latLng']['lat']) + ',' + str(prefferedLocation['latLng']['lng'])
        # print(type(data))
        # print('\n')
        # print(response.status_code)

        # print(data)

    # else:
    #     location = ''

    return location


separator = "\t"
input_file = 'userInfoUCL.tsv'
output_file = 'result/locationUCL.tsv'

CSV_file_in = open(input_file,'r')
userInfos = CSV_file_in.readlines()
CSV_file_in.close()

# userIds = []
# rows = csv.reader(users, delimiter=separator, quotechar='"')
# for row in rows:
#     userIds.append(row[0])

totalUsers = len(userInfos)
printProgressBar(0, totalUsers, prefix = 'Progress:', suffix = 'Complete', length = 50)

CSV_file_out = open(output_file, 'w')
for index, userInfo in enumerate(userInfos):
    printProgressBar(index + 1, totalUsers, prefix = 'Progress:', suffix = 'Complete', length = 50)
    ###########################################################
    ###         DO YOUR FANCY STUFF HERE                    ###
    ##         CSV_file_out.write("hello world" + "\n")    ###
    ###########################################################
    userObject = ast.literal_eval(userInfo)

    userLocation = fetchLocation(userObject)
    CSV_file_out.write(userLocation + "\n")

CSV_file_out.close()
print('')
