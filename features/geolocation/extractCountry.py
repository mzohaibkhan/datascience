import ast
import os
import json
import time
from datetime import datetime, timedelta
import sys
import csv
import re
import pprint
import requests

# sys.path.insert(0,os.environ['DATASCIENCE'])
# import requests
# from apis import TwitterAPIs

pp = pprint.PrettyPrinter(indent=2)

def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total:
        print()

def fetchCountry(geoCoordinates):
    # Default Latitude,Longitude for ucl final
    # location = '50.4334058,30.521856'
    country = 'US'

    if geoCoordinates != '':

        try:
            # Make the same request we did earlier, but with the coordinates of San Francisco instead.
            parameters = { 'location': geoCoordinates, 'key': 'lYrP4vF3Uk5zgTiGGuEzQGwGIVDGuy24' }
            response = requests.get("http://www.mapquestapi.com/geocoding/v1/address", params=parameters)

            # Get the response data as a python object.  Verify that it's a dictionary.
            data = response.json()

            if 'results' in data and len(data['results']) > 0:
                location = data['results'][0]

                if 'locations' in location and len(location['locations']) > 0:
                    prefferedLocation = location['locations'][0]
                    
                    if 'adminArea1' in prefferedLocation:
                        country = str(prefferedLocation['adminArea1'])
        except:
            print("Error occurred")
    return country


separator = "\t"
input_file = 'verified_ucl/locationUCL_verified.tsv'
output_file = 'countries/ucl.tsv'

CSV_file_in = open(input_file,'r')
coordinates = CSV_file_in.readlines()
CSV_file_in.close()

# userIds = []
# rows = csv.reader(users, delimiter=separator, quotechar='"')
# for row in rows:
#     userIds.append(row[0])

totalCoordinates = len(coordinates)
printProgressBar(0, totalCoordinates, prefix = 'Progress:', suffix = 'Complete', length = 50)

CSV_file_out = open(output_file, 'w')
for index, coordinate in enumerate(coordinates):
    printProgressBar(index + 1, totalCoordinates, prefix = 'Progress:', suffix = 'Complete', length = 50)
    ###########################################################
    ###         DO YOUR FANCY STUFF HERE                    ###
    ##         CSV_file_out.write("hello world" + "\n")    ###
    ###########################################################
    # userObject = ast.literal_eval(coordinate)
    coordinate = coordinate.strip('\n')
    # print(coordinate)
    country = fetchCountry(coordinate)
    # print(country)
    CSV_file_out.write(country + "\n")
    CSV_file_out.flush()

CSV_file_out.close()
print('')
