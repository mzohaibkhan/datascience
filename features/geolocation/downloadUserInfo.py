import ast
import os
import json
import time
from datetime import datetime, timedelta
import sys
import csv
import re
import pprint

sys.path.insert(0,os.environ['DATASCIENCE'])
import requests
from apis import TwitterAPIs

# pp = pprint.PrettyPrinter(indent=4)

def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total:
        print()

def getUserInfo(userId):
    location = ''
    userInfo = TwitterAPIs.getUserInfo(userId)
    return str(userInfo)


separator = "\t"
input_file = '../../data/General_Users_Final.txt'
output_file = 'userInfoGeneral.tsv'

CSV_file_in = open(input_file,'r')
users = CSV_file_in.readlines()
CSV_file_in.close()

userIds = []
rows = csv.reader(users, delimiter=separator, quotechar='"')
for row in rows:
    userIds.append(row[0])

totalUsers = len(userIds)
print('Total Users: ' + str(totalUsers))

printProgressBar(0, totalUsers, prefix = 'Progress:', suffix = 'Complete', length = 50)

CSV_file_out = open(output_file, 'w')
for index, userId in enumerate(userIds):

    printProgressBar(index + 1, totalUsers, prefix = 'Progress:', suffix = 'Complete', length = 50)
    ###########################################################
    ###         DO YOUR FANCY STUFF HERE                    ###
    ##         CSV_file_out.write("hello world" + "\n")    ###
    ###########################################################
    userInfo = getUserInfo(userId)
    CSV_file_out.write(userInfo + "\n")

CSV_file_out.close()
