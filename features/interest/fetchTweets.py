participant=False
user_type="general_users"
directory = 'non_participants_result'
import ast
import os
import json
import time
from datetime import datetime, timedelta
import sys
import csv
import re

sys.path.insert(0,os.environ['DATASCIENCE'])
import requests
from apis import TwitterAPIs

def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total:
        print()

def getTweets(userId, count=200, query=""):
    fileText = ''
    tweets = TwitterAPIs.getTweets(userId,count)

    for tweet in tweets:
        tweet_json = tweet._json
        # print(tweet_json)
        if 'text' in tweet_json:
            tweetText = tweet_json['text']
            # print(tweetText)
            hashTags = re.findall(r"#(\w+)", tweetText)
            if len(hashTags) > 0:
                line = ''
                for hashTag in hashTags:
                    line = line + '#' +hashTag + ','

                line = line[:-1]
                fileText = fileText + line + '\n'
    return fileText

# print(getTweets('2769892853'))
# getTweets('2769892853')

separator = "\t"
if participant:
    user_type="ucl_users"
    directory = 'participants_result'
input_file = '../../data/'+user_type+'.tsv'

print("fetching tweets for interest calculation: user_type = "+user_type+", output directory = "+directory)
if not os.path.exists(directory):
    os.makedirs(directory)
# output_file = 'userGenders.csv'


CSV_file_in = open(input_file,'r')

users = CSV_file_in.readlines()
CSV_file_in.close()

userIds = []

rows = csv.reader(users, delimiter=separator, quotechar='"')
for row in rows:
    userIds.append(row[0])

# userIds = userIds[0:4]
totalUsers = len(userIds)
printProgressBar(0, totalUsers, prefix = 'Progress:', suffix = 'Complete', length = 50)

for index, userId in enumerate(userIds):

    printProgressBar(index, totalUsers, prefix = 'Progress:', suffix = 'Complete', length = 50)
    CSV_file_out = open(directory + "/" + userId,'w')

    ###########################################################
    ###         DO YOUR FANCY STUFF HERE                    ###
    ##         CSV_file_out.write("hello world" + "\n")    ###
    ###########################################################
    CSV_file_out.write(getTweets(userId))

    CSV_file_out.close()
print()