# import any 
import sys
import os
sys.path.insert(0,os.environ['DATASCIENCE'])
import ast
import json
import time
import requests
import ast
from apis import TwitterAPIs
from features_extraction_helpers import features_extracter
from datetime import datetime, timedelta
import csv


participant=False
user_type="general_users"
directory = 'results'
output_file='non_partitipant_friends_percentage.tsv'


if participant:
    user_type="ucl_users"
    output_file='partitipant_friends_percentage.tsv'


separator = "\t"
input_file = '../../data/'+user_type+'.tsv'
if not os.path.exists(directory):
    os.makedirs(directory)

CSV_file_in = open(input_file,'r')



lineString = CSV_file_in.readlines()
CSV_file_in.close()
rows = csv.reader(lineString, delimiter=separator, quotechar='"')
#for number of users
users_limit = 100
users_count = 0
CSV_file_out = open(directory + "/" +output_file,'w')
CSV_file_out.write('id'+separator+'friends_percentage'+"\n")
for value in rows:
	id = value[0]
	screen_name = value[1]
	print(str(id) + " : " +screen_name)
	if users_count > users_limit:
		break
	users_count = users_count + 1
	print("Processing user: " + screen_name)
	friends_list = TwitterAPIs.getFriendIds(id,100)
	total_participant_friend_percentage = 0
	for friend in friends_list:
		participated = features_extracter.hasUserParticipated(friend)
		print(str(friend)+" participated: "+str(participated))
		if participated:
			total_participant_friend_percentage = total_participant_friend_percentage + 1
	CSV_file_out.write(str(id)+separator+str(total_participant_friend_percentage)+"\n") # as total users being taken in to account are 100, to number of users participated from those 100 users are used as the percentage participants

CSV_file_out.close()
###########################################################
###         DO YOUR FANCY STUFF HERE                    ###
###         CSV_file_out.write("hello world" + "\n")    ###
###########################################################

    
