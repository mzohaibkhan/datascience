import sys
import os
sys.path.insert(0,os.environ['DATASCIENCE'])

import ast
import json
import time
from datetime import datetime, timedelta
import csv
import re
import pprint
import requests
import tweepy
import authentication_token

import requests
from apis import TwitterAPIs

def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total:
        print()

def fetchFavorites(userId):
    id = str(userId)
    try:
        return api.favorites(id=id, count=100)
    except:
        print("Error occurred in get user favorites")
        return []

def getUserFavorites(userId):
    fileText = ''
    tweets = fetchFavorites(userId)
    
    try:
        if(len(tweets) > 0):
            for tweet in tweets:
                tweetObj = tweet._json
                
                if tweetObj['lang'] == 'en':
                    tweetText = tweetObj['text']
                    
                    hashTags = re.findall(r"#(\w+)", tweetText)
                    
                    if len(hashTags) > 0:
                        line = ''
                        for hashTag in hashTags:
                            line = line + '#' +hashTag + ','

                        line = line[:-1]
                        fileText = fileText + line + '\n'
    except:
        print("Some error occurred")

    return fileText
    


consumer_key = authentication_token.client_id
consumer_secret = authentication_token.client_secret
access_token = authentication_token.user_token
access_token_secret = authentication_token.user_token_secret

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
api = tweepy.API(auth,wait_on_rate_limit=True)

separator = "\t"
input_user_file = '../../data/ucl_users.tsv'
# input_userInfo_file = 'userInfoGeneral.tsv'
# input_location_file = 'result/locationUCL.tsv'
# output_location_file = 'verified_general/locationGeneral.tsv'
# output_user_fav_file = 'favoriteTweets/UCL_favorites.tsv'
directory = 'UCL_favorites'
if not os.path.exists(directory):
    os.makedirs(directory)

user_file = open(input_user_file,'r')
userIdCsv = user_file.readlines()
user_file.close()


# fav_file_out = open(output_user_fav_file, 'w')

userIds = []
rows = csv.reader(userIdCsv, delimiter=separator, quotechar='"')
for row in rows:
    userIds.append(row[0])

# a[start:end]  items start through end-1
# a[start:]     items start through the rest of the array
# a[:end]       items from the beginning through end-1
# a[:]          a copy of the whole array
# First 1000 users

userIds = userIds[2001:3001]

totalUsers = len(userIds)
printProgressBar(0, totalUsers, prefix = 'Progress:', suffix = 'Complete', length = 50)

for index, userId in enumerate(userIds):
    printProgressBar(index + 1, totalUsers, prefix = 'Progress:', suffix = 'Complete', length = 50)
    ###########################################################
    ###         DO YOUR FANCY STUFF HERE                    ###
    ###        CSV_file_out.write("hello world" + "\n")     ###
    ###########################################################

    CSV_file_out = open(directory + "/" + userId,'w')
    CSV_file_out.write(getUserFavorites(userId))
    # favorites = getUserFavorites(userId)
    CSV_file_out.close()
    # print(favorites)
    # location_file_out.write(location + "\n")
    # location_file_out.flush()

# location_file_out.close()
