import csv
import os.path

separator = "\t"
favorites_feture_file = 'Feature_CSV/favorites_feture_file.tsv'
ff_writer = open(favorites_feture_file,'w')
ff_writer.write("user_id"+separator+"favorites\n")
# favorites = getUserFavorites(userId)
input_file = '../../data/ucl_users.tsv'
# sports_file = 'sport-tags.txt'
input_directory = 'UCL_favorites'
# output_file = 'userInterests.tsv'

users_file_in = open(input_file,'r')
# sports_file_in = open(sports_file,'r')
# CSV_file_out = open(output_file,'w')

def isUclFinalRelated(userTweetCSV):
    # print(userTweetCSV)
    userTweetCSV = userTweetCSV.lower()
    tags = userTweetCSV.split(',')
    flag = False
    # print(tags)
    for tag in tags:
        if tag in uclTags:
            flag = True
            break
    return flag


usersLines = users_file_in.readlines()
users_file_in.close()

# sportTagLines = sports_file_in.readlines()
# sports_file_in.close()

userIds = []
uclTags = ['#ucl', '#uclfinal']

users = csv.reader(usersLines, delimiter=separator, quotechar='"')
for row in users:
    userIds.append(row[0])

for index, userId in enumerate(userIds):
    tagCount = 0

    hashTagsUserFile = input_directory + "/" + userId
    
    if os.path.isfile(hashTagsUserFile):
        userFile = open(input_directory + "/" + userId, 'r')
        userTweets = userFile.readlines()
        userFile.close()

        totalTweets = len(userTweets)

        if totalTweets > 0:
            # print(userTweets[-1])
            for userTweet in userTweets:
                userTweet = userTweet.strip('\n')

                if isUclFinalRelated(userTweet):
                    tagCount = tagCount + 1
        # else:
        #     print('Empty file')

    # else:
    #     print('No file Exist')
    ff_writer.write(str(userId)+separator+str(tagCount)+"\n")
    print(str(userId)+" related favorites = "+str(tagCount))

        # CSV_file_out.write(userId + separator + str(percentInterest) + "\n")
        
# CSV_file_out.close()
ff_writer.close()
