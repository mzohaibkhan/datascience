#Extract users From Tweets
import ast
import os
import json
import time
from datetime import datetime, timedelta
import re

def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total:
        print()

separator = "\t"
input_file = '../data/UCLfinal.txt'
output_file = '../data/users.tsv'
total_items = 10000

CSV_file_in = open(input_file,'r')
CSV_file_out = open(output_file,'w')

lineString = CSV_file_in.readlines()
CSV_file_in.close()

users = set()

printProgressBar(0, total_items, prefix = 'Progress:', suffix = 'Complete', length = 50)

for value in lineString:
    printProgressBar(len(users), total_items, prefix = 'Progress:', suffix = 'Complete', length = 50)

    if len(users) > total_items - 1: # Max limit
        break

    json_str = ast.literal_eval(value)

    if json_str['user']['geo_enabled']: # Geo Enabled users
        fullName = json_str['user']['name'].split(' ', 1)
        if len(fullName) > 1: # Have Full Name
            if not re.search('[^a-zA-Z0-9-_.\s]', json_str['user']['name']): # Should not contain special characters
                output = str(json_str['user']['id']) + separator + json_str['user']['screen_name'] + separator + json_str['user']['name']
                users.add(output)

for value in users:
    CSV_file_out.write(value + "\n")

CSV_file_out.close()
print("Wrote " + str(len(users)) + " unique users from " + str(len(lineString)) + " tweets")
