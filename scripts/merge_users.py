


def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total:
        print()

total_items = 10000
separator = "\t"
input_file_1 = '../data/general-users.tsv'
input_file_2 = '../data/general-users_1.tsv'
output_file = '../data/General_Users_Final.txt'

users = set()

CSV_file_out = open(output_file,'w')

CSV_file_in_1 = open(input_file_1,'r')
lineString_1 = CSV_file_in_1.readlines()
CSV_file_in_1.close()

CSV_file_in_2 = open(input_file_2,'r')
lineString_2 = CSV_file_in_2.readlines()
CSV_file_in_2.close()




printProgressBar(0, total_items, prefix = 'Progress:', suffix = 'Complete', length = 50)
i = 0;
for value in lineString_1:
    printProgressBar(len(users), total_items, prefix = 'Progress:', suffix = 'Complete', length = 50)

    if i%2 == 0:
    	users.add(value)
    
    if len(users) > total_items/2 - 1: # Max limit
        break


# print()
# printProgressBar(0, total_items, prefix = 'Progress:', suffix = 'Complete', length = 50)

for value in lineString_2:
    printProgressBar(len(users), total_items, prefix = 'Progress:', suffix = 'Complete', length = 50)
    if i%2 == 0:
    	users.add(value)
    if len(users) > total_items - 1: # Max limit
        break

for value in users:
    CSV_file_out.write(value)
print()
CSV_file_out.close()
print("Merged " + str(len(users)) + " unique users from " + str(len(lineString_1)+len(lineString_2)) + " users")
