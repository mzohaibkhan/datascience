# -*- coding: utf-8 -*-
from scipy import sparse
import classifiers as cl

x, y = cl.loadData()

# Now, the dataset should be split in to train and test sets
X_train, X_test, y_train, y_test = cl.splitTestTrain(x, y, 0.20)

print("\n\nStart training")
#precison, recall and f-measure of all the 4 classifiers
# naiveBayesPrecision, naiveBayesRecall, naiveBayesFMeasure = cl.applyNaiveBayesClassifierTuning(X_train, y_train, X_test, y_test)
# svmPrecision, svmRecall, svmFMeasure = cl.applySVMClassifier(X_train, y_train, X_test, y_test)
# randomForestPrecision, randomForestRecall, randomForestFMeasure = cl.applyRandomForestClassifier(X_train, y_train, X_test, y_test)
# nnPrecision, nnRecall, nnFMeasure = cl.applyNeuralNetworkClassifier(X_train, y_train, X_test, y_test)

cl.applyDecisionTreeClassifierTuning(X_train, y_train, X_test, y_test)

cl.applyRandomForestClassifierTuning(X_train, y_train, X_test, y_test)

cl.applyNaiveBayesClassifierTuning(X_train, y_train, X_test, y_test)

cl.applyNeuralNetworkClassifierTuning(X_train, y_train, X_test, y_test)

cl.applySVMClassifierTuning(X_train, y_train, X_test, y_test)


# Plot Precision-Recall comparison graph
# cl.plotPreRec(naiveBayesRecall, naiveBayesPrecision, svmRecall, svmPrecision, randomForestRecall, randomForestPrecision, logisticRegressionRecall, logisticRegressionPrecision, sgdRecall, sgdPrecision)

# plot FMeasure comparison graph
# cl.plotAcuuracyComaprisonGraph(naiveBayesFMeasure, svmFMeasure, randomForestFMeasure, nnFMeasure)

