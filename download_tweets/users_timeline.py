# %matplotlib inline
import requests
from dateutil import parser
import pandas as pd
import seaborn as sns
import numpy as np
import base64


client_id = authentication_token.client_id
client_secret = authentication_token.client_secret
user_token = authentication_token.user_token
user_token_secret = authentication_token.user_token_secret


def get_access_token(clientid, clientsecret):
    credentials = clientid+":"+clientsecret
    
    # encode credentials with base64
    base64EncodedCredentials = base64.b64encode(credentials.encode()).decode()

    # prepare request params / header
    params = {"grant_type":"client_credentials"}
    header = {"Content-Type":"application/x-www-form-urlencoded;charset=UTF-8",
              "Authorization":"Basic " + base64EncodedCredentials}

    # request access token (bearer token)
    r = requests.post("https://api.twitter.com/oauth2/token", data = params, headers = header)
        
    if r.status_code == requests.codes.ok:
        # print(r.json())
        # print("Successfull. I obtained the following token type: %s" % r.json()["token_type"])
        return r.json()["access_token"]
    else:
        print("Error. Could not connect to twitter:", r)
        raise IOError("Could not access Twitter. Maybe invalid credentials?")

# access_token = get_access_token(client_id, client_secret)
access_token="AAAAAAAAAAAAAAAAAAAAAFTz4gAAAAAADu92hG9itFvDGLVM0WpJofVFwCk%3De0CRx9V7PFMW8nHWiXXl0x3wfzHwqug53Jq83hrPRMQ09xKHZ6"
print(access_token)


def timeline(screen_name, count=20, keep_retweets=True, text_only=True, query="UFCfinal"):
    params = {"screen_name" : screen_name,
              "q" : query,
              "count" : count,
              "tweet_mode": "extended"}             
    header = {"Authorization" : "Bearer {}".format(access_token), 'Accept-Encoding': 'gzip'}
    r = requests.get("https://api.twitter.com/1.1/statuses/user_timeline.json", params=params, headers=header)
    return r.json()

def parse_tweets(tweets, keep_retweets=True):
    ''' Extract some fields from tweet objects. '''
    if not keep_retweets:
        tweets = [s for s in tweets if not 'retweeted_status' in s]
    
    return [(s["full_text"], s["user"]["screen_name"], s["created_at"]) for s in tweets]





tweets = timeline("sjithompson", count=100, text_only=False)
parsed_tweets = parse_tweets(tweets)

print(parsed_tweets)
print(len(parsed_tweets))
