import tweepy
import sys
import os
sys.path.insert(0,os.environ['DATASCIENCE'])
import authentication_token
# Consumer keys and access tokens, used for OAuth
consumer_key = authentication_token.client_id
consumer_secret = authentication_token.client_secret
access_token = authentication_token.user_token
access_token_secret = authentication_token.user_token_secret

# OAuth process, using the keys and tokens
auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)

# Creation of the actual interface, using authentication
api = tweepy.API(auth)
count = 0
for status in tweepy.Cursor(api.user_timeline, screen_name='@sjithompson').items():
    print(status.created_at)
    count = count + 1
print(count)