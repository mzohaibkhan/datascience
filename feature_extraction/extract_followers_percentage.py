# import any 
import sys
import os
sys.path.insert(0,os.environ['DATASCIENCE'])
import ast
import json
import time
import requests
import ast
from scripts import TwitterAPIs
from scripts import Features_Extracter
from datetime import datetime, timedelta


separator = "\t"
input_file = '../users.csv'
directory = 'extracted_features'
if not os.path.exists(directory):
    os.makedirs(directory)

CSV_file_in = open(input_file,'r')

lineString = CSV_file_in.readlines()
CSV_file_in.close()

#for number of users
users_limit = 100
users_count = 0
CSV_file_out = open(directory + "/" +"followers_percentage.tsv",'w')
CSV_file_out.write('id'+separator+'followers_percentage'+"\n")
for value in lineString:
	if users_count > users_limit:
		break
	users_count = users_count + 1
	value = value.strip('\n')
	print("Processing user: " + value)
	# print(r.json())
	friends_list = TwitterAPIs.getFriendIds(value,200)
	for friend in friends_list:
		friends_tweets = TwitterAPIs.get_user_tweets(friend)
		print(str(friend)+" participated: "+str(Features_Extracter.hasUserParticipated(value,query="UCLfinal")))
		# print("Friend's Tweets: "+str(friends_tweets))


	# CSV_file_out.write(str(json_str['id'])+separator+str(json_str['name'])+separator+str(json_str['location'])+separator+str(json_str['followers_count'])+separator+str(json_str['friends_count'])+separator+str(json_str['favourites_count'])+"\n")

CSV_file_out.close()
###########################################################
###         DO YOUR FANCY STUFF HERE                    ###
###         CSV_file_out.write("hello world" + "\n")    ###
###########################################################

    
