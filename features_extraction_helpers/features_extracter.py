import sys
sys.path.insert(0,'/Users/zohaib/Development/workspace/datascience')
import requests
from apis import TwitterAPIs
import authentication_token
import tweepy


consumer_key = authentication_token.client_id
consumer_secret = authentication_token.client_secret
access_token = authentication_token.user_token
access_token_secret = authentication_token.user_token_secret

# OAuth process, using the keys and tokens
auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)

# Creation of the actual interface, using authentication
api = tweepy.API(auth)


def hasUserParticipated2(screen_name):
	print("\n\n\n\n\n============ getting tweets for user =========="+str(screen_name))
	for status in tweepy.Cursor(api.user_timeline, screen_name=screen_name).items():
		print(status.text)

def hasUserParticipated(id, count=100, keep_retweets=True, text_only=True, query="UCLfinal"):
    print("\n\n\n\n\n============ getting tweets for user =========="+str(id))
    tweets = TwitterAPIs.get_user_tweets(id,100,query)
    print(tweets)
    for tweet in tweets:
    	# print(tweet)
    	if query.lower() in tweet["full_text"].lower():
    	 	return True
    return False
